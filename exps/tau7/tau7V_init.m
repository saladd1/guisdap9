% cp1lR_init.m: Initialization for cp1lr
% GUISDAP v1.65
% 
% Radar frequency and basic time unit
% Klystron installed 2000: 926-930.5 MHz
                                     
ch_fradar=[224.0e6 224.0e6];
p_dtau=4.0;
