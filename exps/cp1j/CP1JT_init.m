% GUISDAP v1.50   94-03-10 Copyright Asko Huuskonen, Markku Lehtinen 
% 
% CP1KT_init.m
% Radar frequency and basic time unit
                                     
ch_fradar=931.5e6*ones(1,8);
p_dtau=1.0;
