% PS_SETFREQ.m: ps file interpreter routine
% GUISDAP v1.60   96-05-27 Copyright Asko Huuskonen, Markku Lehtinen
%
% [ch f] ;PS_SETFREQ
r=ans;
ch_f(r(1))=r(2);
