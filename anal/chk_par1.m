% chk_par1: Interface betwen the user supplied and internal control parameters 
% GUISDAP v.1.81 03-02-27 Copyright EISCAT, Huuskonen&Lehtinen
% 
% chk_par1 is the main interface between the user supplied control parameters and
% the internal GUISDAP control parameters. There is mostly a one-to-one correspondence
% between the parameters e.g.
% User parameter   GUISDAP parameter
% analysis_altit    a_altit
% analysis_integr   a_integr
% display_figures   di_figures
% integ_deffile     a_integdeffile
% The user parameters are local to the workspace whereas the GUISDAP parameters are global
% If any of the user parameters is not specified, or empty, the corresponding GUISDAP
% parameter will get the values specified in the routine.
%
% See also: an_start globals chk_par2

% make sure that the various path names have directory separators at the end
data_path=fullfile(deblank(strjust(data_path,'left')),filesep);
result_path=fullfile(deblank(strjust(result_path,'left')),filesep);
path_GUP=fullfile(path_GUP,filesep);
path_exps=fullfile(path_exps,filesep);
path_tmp=fullfile(path_tmp,filesep);
if exist('analysis_plasmaline','var') & analysis_plasmaline
 display_figures(2:5)=[];
 analysis_do=0;
 d_saveint.var=0;
 d_saveint.dir=result_path;
 if exist('plasma_range','var'), d_saveint.range=plasma_range; end
end
if ~isempty(d_saveint)
 d=strfind(d_saveint.dir,'AUTO');
 if length(d)==1
  d_saveint.dir=[d_saveint.dir(1:d-1) sprintf('%s_%s%d_%d@%s',datestr(analysis_start,29),name_expr,expver,floor(analysis_integr(1)),name_site)];
  if isfield(d_saveint,'range')
   d_saveint.dir=[d_saveint.dir 'p'];
  end
 end
 d_saveint.dir=fullfile(d_saveint.dir,filesep);
 if ~exist(d_saveint.dir,'dir')
  [i,j]=fileparts(d_saveint.dir(1:end-1));
  if isempty(i), mkdir(j), else, mkdir(i,j), end
 end
 if ~isfield(d_saveint,'var')
  d_saveint.var=1;
 end
end

% The first if-block tries to locate the data source and produces variables 
% necessary for integration:

a_simul=[];
if exist('analysis_simul','var')
 % The second branch is for simulated data to be calculated from the theory
 % The presence of the variable a_simul is the flag, which is used later
 % a_simul(1): integration time
 % a_simul(2): controls the start time in steps of 7200 s, i.e. 2 hours.
 % a_simul(3): The transmitter power
 % a_simul(4): The background temperature
 % a_simul(5:7): Antenna range, azimuth and elevation
 % a_simul(8): The calibration temperature
 a_simul=[0 1 1.2e6 100 300 180 90 210];
 a_simul(1:length(analysis_simul))=analysis_simul;
 a_year=2000;
 a_start=7200*a_simul(2);
 a_end=7200*a_simul(2)+a_simul(1);
 analysis_control(4)=2;
else
 if exist('analysis_integr','var')
  a_integr=analysis_integr;
 else
  a_skip=zeros(size(a_integr));
 end
 if exist('analysis_realtime','var')
  a_realtime=analysis_realtime;
 else
  a_realtime=0;
 end
 if exist('analysis_txlimit','var')
  a_txlimit=analysis_txlimit;
 else
  a_txlimit=100e3;
 end
 if exist('analysis_skip','var')
  a_skip=analysis_skip;
 else
  a_skip=zeros(size(a_integr));
 end
 if ~exist('recurse','var'), recurse=[]; end 

 a_rawdata=0;
 if exist('analysis_rawdata','var')
  a_rawdata=analysis_rawdata;
 end
 a_start=timeconv(analysis_start,'utc2tai'); % the programs uses internally TAI
 a_end  =timeconv(analysis_end,'utc2tai');
 a_year=analysis_start(1);
 if a_rawdata==1, % HDF5 format
  d_filelist=h5read(data_path,'/Time/MatlabTime');
  i=datevec(d_filelist(1));
  d_filelist=(d_filelist-datenum(i(1),1,1))*86400;
 elseif name_site=='Q'
  % The data are binary Quijing files
  global a_timestamp qmeta
  a_timestamp=[3.9 6.2];
  if exist('analysis_timestamp','var')
   a_timestamp(1:length(analysis_timestamp))=analysis_timestamp;
  end
  load(fullfile(path_GUP,'matfiles','q_metadata'))
  [d_filelist,msg]=qetfilelist(data_path);
  if ~isempty(msg)
   error(msg)
  end
 else
  % The normal case where it is expeced the data are matlab files
  if ~a_realtime
    recurse(strfind(recurse,'?'))='*';
  end
  i=strfind(recurse,'**');
  for i=fliplr(i)
    recurse(i+1)=[];
  end
  if isempty(dir([data_path '*.mat*']))
    data_path=fullfile(data_path,recurse,filesep);
  end
  [d_filelist,msg]=getfilelist(data_path);
  if ~isempty(msg)
   error(msg)
  end
  if ~isfield(d_filelist,'tai')
    d=cell2mat({d_filelist.file})';
    d=num2cell(timeconv([a_year*ones(size(d)) d],'gup2tai'));
    [d_filelist.tai]=d{:};
  end
 end
 a_ind=0;
end

a_control=[-1 0.01 100 1];
% a_control(1)  No fit is tried, if the error of Ne is larger than (1) at the start
% a_control(2)  Fitting is stopped when step for all parameters is less than (2)
% a_control(3)  Maximum number of iterations
% a_control(4)  Variance calculation
%               = 1 when variance estimated from data
%               = 1.5 use ambiguity funcs if no estimates low for data var calcs
%               = 2 when variance estimated using ambiguity functions
if exist('analysis_control','var')
 ind=find(analysis_control>0);
 a_control(ind)=analysis_control(ind);
end

a_Magic_const=1;
if exist('Magic_const','var')
 a_Magic_const=Magic_const;
end
a_NCAR=0;
if a_realtime & isunix & ~isempty(local.site)
 %a_NCAR=3;
end
if exist('NCAR','var')
 NCAR_output
 a_NCAR=NCAR;
end
a_HDF=0;
if exist('analysis_HDF','var')
 a_HDF=analysis_HDF;
end
if exist('analysis_code','var')
 a_code=analysis_code;
end

di_figures=[0 0 0 0 0]; di_results=0; di_spectra=0;
if name_site=='S' | name_site=='K', di_results=1; end
if exist('display_figures','var')
 di_figures(1:length(display_figures))=display_figures;
end
if exist('display_results','var')
 di_results=display_results; 
end
if exist('display_spectra','var')
 di_spectra=display_spectra; 
end

a_save=1;
if exist('analysis_save','var')
 a_save=analysis_save;
 if ~a_save
  a_NCAR=0; di_figures(5)=0; result_path=path_tmp;
 end
end
a_savespec=0;
if exist('analysis_savespec','var')
 a_savespec=analysis_savespec;
end

a_do=1;
if exist('analysis_do','var')
 a_do=analysis_do;
end
a_intfixed=1;
if exist('analysis_intfixed','var')
 a_intfixed=analysis_intfixed;
end
a_intallow=[.2 .2 1000 0];
if exist('analysis_intallow','var')
 a_intallow(1:length(analysis_intallow))=analysis_intallow;
end
a_intfix=[9 10 42 64];
if exist('analysis_intfix','var')
 d=find(analysis_intfix);
 a_intfix(d)=analysis_intfix(d);
 a_intallow(length(a_intallow)+1:length(a_intfix))=0;
end
if exist('analysis_intfixforce','var')
 a_intfixforce=analysis_intfixforce;
end
if exist('analysis_sweep','var')
 if strfind(analysis_sweep,'az')
  a_intallow(2)=Inf;
 elseif ~strfind(analysis_sweep,'el')
  warning('GUISDAP:parse','analysis_sweep has to contain ''el'' and/or ''az'' strings')
 end
 if strfind(analysis_sweep,'el')
  a_intallow(1)=Inf;
 end
end
if exist('analysis_txpower','var')
 a_txpower=analysis_txpower;
elseif name_site=='V' & a_year>2006
 a_txpower=[70 1000];
 warning('GUISDAP:chkpar','Using measured tx power, override with analysis_txpower=8;')
elseif name_site~='V' & a_year>2004
 a_txpower=[65 1000];
 warning('GUISDAP:chkpar','Using measured tx power, override with analysis_txpower=8;')
else
 a_txpower=[8 1];
end
if length(a_txpower)==1
 if a_txpower(1)==8, a_txpower(2)=1; else, a_txpower(2)=1e3; end
end
sysTemp=[];
if exist('analysis_Tsys','var')
 sysTemp=analysis_Tsys;
end
a_rcprog=1;
if exist('analysis_rcprog','var')
 a_rcprog=analysis_rcprog;
end
a_ppshortlags=0;
if exist('analysis_ppshortlags','var')
 a_ppshortlags=analysis_ppshortlags;
end
if length(a_ppshortlags)==1
 a_ppshortlags(2)=Inf;
end
a_gating=0;
if exist('analysis_gating','var')
 a_gating=analysis_gating;
end
a_pponly=0;
if exist('analysis_pponly','var')
 a_pponly=analysis_pponly;
end
a_ppcombine=0;
if exist('analysis_ppcombine','var')
 a_ppcombine=analysis_ppcombine;
end
a_lpf(1).do=0;
if exist('analysis_lpf','var')
 a_lpf=analysis_lpf;
end
if a_lpf(1).do
 chk_lagprofiling()
end

if exist('display_analysis_pars','var')
 empty_pars=[];
 for i=who('a_*','d_s*','path_*')'
  j=char(i);
  if isempty(eval(j))
   empty_pars=[empty_pars ' ' j];
  else
   eval(j)
  end
 end
 if ~isempty(empty_pars)
  fprintf('Empty parameters:%s\n',empty_pars)
 end
 fprintf('<Press return>')
 pause
end
