% clear_results.m: defines the result variables as empty matrices
% GUISDAP v.1.60 96-05-27 Copyright Asko Huuskonen and Markku Lehtinen
%
% function clear_results
  function clear_results

global r_range r_param r_error r_res r_status r_dp r_w
global r_apriori r_apriorierror r_spec r_om r_freq r_acf

r_range=[];r_param=[];r_error=[];r_res=[];r_status=[];r_dp=[]; r_w=[];
r_apriori=[];r_apriorierror=[];r_spec=[];r_om=[];r_freq=[]; r_acf=[];
