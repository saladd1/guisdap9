% GUIZARD.m: script for GUIZARDs and other power users to change GUP variables. 
% GUISDAP v.1.60 96-05-27 Copyright Asko Huuskonen and Markku Lehtinen
%
% script for GUIZARDs and other power users. Put here any commands by
% which you wish to change the GUP variables and ambiguity functions before
% the program proceeds to data analysis
%
% See also: GUISPERT
%
if ~exist('p_ND'), p_ND=1; end

